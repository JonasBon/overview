**Install**
Install the swift binary 
```cp C:\Library\Developer\Platforms\Windows.platform\Developer\SDKs\Windows.sdk\usr\lib\swift\windows C:\Library\Developer\Toolchains\unknown-Asserts-development.xctoolchain\usr\lib\swift```


**Compile**
```swiftc -o main.exe main.swift```
(https://forums.swift.org/t/compiling-a-simple-hello-swift-on-windows/40534/3)[https://forums.swift.org/t/compiling-a-simple-hello-swift-on-windows/40534/3]
