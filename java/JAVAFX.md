Gradle Setup
=====
1. run `gradle init`
2. make sure your `build.gradle` file contains:
 ```js
    plugins {
        id 'application'
        id 'org.openjfx.javafxplugin' version '0.0.9'
    }

    application {
        mainClass = 'com.example.your_package.MainClass'
    }

    javafx {
        version = "11.0.1"
        modules = ['javafx.controls', 'javafx.fxml'] // and other modules you need
    }

    sourceSets {
        main {
            resources {
                srcDirs = ["src/main/java"]
                includes = ["**/*.fxml"]
            }
        }
    }
```

Gradle Run
=====
## Comandline
Run `gradle run` from your project root (containing *app* and *settings.gradle*)

## IntelliJ IDEA Run Configuration
1. Run Menu > Edit Configurations > + > Gradle
2. Choose gradle project by clicking the folder icon
3. Add `run` to Tasks
4. Apply > Ok
5. Run or Shift+F10

Java Run
=====
## IntelliJ IDEA Run Configuration
1. Download [JavaFX](https://gluonhq.com/products/javafx/) and extract to `C:\Program Files\Java\` or whereever you like
2. Run Menu > Edit Configurations > + > Application
3. Select SDK
4. Select classpath `your_project.app.main`
5. Add VM options (via modify options)
    ```--module-path
    "C:\Program Files\Java\javafx-sdk-11.0.2\lib"
    --add-modules
    javafx.controls,javafx.fxml,javafx.swing```
6. Select main class `com.your_package.App`
7. Select working directory `your_project_root/app` for consitency with gradle
8. Apply > Ok
9. Run or Shift+F10
