## 1. 
```sh
cd ~/catkin_ws
```

## 2. 
```sh
catkin_create_pkg tf_publisher roscpp tf2_ros tf2_geometry_msgs
```

## 3.
```sh
cd ~/catkin_ws/src/tf_publisher
```

## 4.
```sh
touch src/transformer.cpp
```

## 5. 
```cpp
#include <ros/ros.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "my_awesome_tf_broadcaster");
    tf2_ros::TransformBroadcaster br;
    geometry_msgs::TransformStamped out_tf;

    double alpha 0.0;

    ros::Rate rate(4.0 /* Hz */);

    while (ros::ok()) {
        out_tf.header.stamp = ros::Time::now();
        out_tf.header.frame_id = "world";
        out_tf.child_frame_id = "base_link";
        out_tf.transform.translation.x = cos(alpha) * 1.0f;
        out_tf.transform.translation.y = sin(alpha) * 1.0f;
        alpha += 1.0;
        out_tf.transform.rotation.w = 1;
        br.sendTransform(out_tf);

        ros::spinOnce();
        rate.sleep();
    }
}
```

## 6. 
```
/* 136 */ add_executable(node src/transformer.cpp)

/* 142 */ set_target_properties(node PROPERTIES OUTPUT_NAME node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
/* 146 */ add_dependencies(node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
/* 149 */ target_link_libraries(node
    ${catkin_LIBRARIES}
)```
7.
launch/launch.launch
```xml
<launch>
    <node pkg="tf_publisher" type="node" name="node" output="screen" />
</launch>
```

## 7. 

8.
```sh
rostopic echo /tf
```
