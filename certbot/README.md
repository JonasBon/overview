# Install a certbot certificate for nginx
```
apt install certbot
systemctl stop nginx
certbot certonly -d domain.com

//nginx config

    listen [::]:443 ssl; 
    listen 443 ssl; 
    ssl_certificate /etc/letsencrypt/live/domain.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/domain.com/privkey.pem;

systemctl start nginx
```
courtesy of [**Just**Greg](https://smeli.club)
