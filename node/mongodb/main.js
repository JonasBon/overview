const {MongoClient}  = require('mongodb');
const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB
} = process.env;

async function run() {
    console.log('Started app');

    const mongo_url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;

    const client = new MongoClient(mongo_url);

    await client.connect();

    console.log('Connected to DB');

    let sample_guides = client.db("sample_guides");
    let comets = sample_guides.collection("comets");
    await comets.insertOne({text: 'Test 1'});

    console.log('inserted');

    console.log('finding:');

    console.log(await comets.find({}).project({_id: true, text: true}).toArray());



    await client.close();
}

run().then();
