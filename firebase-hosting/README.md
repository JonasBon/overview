## Set up Firebase Hosting for your app
### 1. Go to [Firebase](https://console.firebase.google.com/u/1/project/_/hosting) and sign in with your Google account
### 2. Create a new Project
* Enter a name, it does not matter what you choose.
* **CHANGE YOUR PROJECT-ID**: Just below the name, there is likely a small badge showing your project id (e.g. `example-project-fffc3`). You should click this and provide a nice name yourself, that is also globally unique. This will be part of the url for accessing your page. Choose wisely, this id **can not be changed later**.
* Enable Google Analytics or don't, it's your choice.
### 3. You will see that your project has the *spark plan* this is the correct one
### 4. Get started with hosting on the Hosting dashboard under Develop > Hosting
### 5. Click start now and follow the steps
 #### Install the firebase CLI
```sh
sudo npm install --global firebase-tools
```
#### Log in to your account and authorize the CLI
```sh
firebase login
```
#### Initialize the project
Just select Hosting `Hosting: Configure files for Firebase Hosting`.  
If using Vue, your public directory is called `dist` this is the default for Vue and I recommend setting firebase up to use this directory
```sh
firebase init
```
### Set up a deployment script and deploy your app
Insert the following property to the `script` object inside your `package.json` file:
```json
"deploy": "npm run build && firebase deploy"
```
Run this script 
```sh
npm run deploy
```
or manually build and deploy your app
```sh
npm run build
firebase deploy
```

## Get a domain
You can get a free domain from [Freenom](https://www.freenom.com/en/index.html?lang=en).

## Set up Nameservers for your Domain
**Update:** You might want to use the free [ClouDns](https://www.cloudns.net/) services if you do not already have a Hetzner account. Add your domain under the DNS Hosting category and add the NS-record IPs to your Domain as described below.

We will be using Hetzner nameservers for this tutorial as they are easy to use and free.
1. Go to [https://dns.hetzner.com/](https://dns.hetzner.com/) and create an account or sign in
2. Add a new zone, using your domain as the zone name
3. On Freenom, go to Services | My Domain > Manage Domain > Management Tools | Nameservers > Use custom nameservers and enter the three nameservers provided by Hetzner:
`helium.ns.hetzner.de`
`hydrogen.ns.hetzner.com`
`oxygen.ns.hetzner.com`

## Add your domain to Firebase
1. Add a custom Domain in your Firebase console
2. Add the full domain (including subdomains) that you want to access your app from, you do not needto forward to another website
3. Create a new TXT-record in the Hetzner DNS console, use your chosen subdomain without your domain (e.g. `app` in `app.example.com`) as the name and the value as provided by Google Firebase
4. Add the A-record as provided by Google Firebase

Your site will be ready right away. Though, the certificate may take a couple hours to be provied. During this period, try accessing your site via http, instead of https.
