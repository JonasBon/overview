```shell
# Set default user shell
sudo useradd -D -s /bin/bash

# Change a user's shell
chsh -s <shell e.g. /bin/bash> <user>

# Add user
useradd --create-home --user-group [--groups <supplementary group> [--groups <another group>]]
useradd -mU [-G <supplementary group>] <user>

# list all user and group ids
getent group

# add user to group
usermod --append --groups <group> [--groups <group>...] <user>
usermod -aG <group> [-G <group>...] <user>
# if you are trying to add a sudoer, remember to delete the password (unless they are using it)
sudo passwd <user> -d

# change group of directory
chgrp <group> <files...>
```

## Firewall
```
firewall-cmd [--permanent] --add-port 1191/tcp
```

[https://www.ibm.com/docs/en/spectrum-scale/5.0.5?topic=firewall-examples-how-open-ports](https://www.ibm.com/docs/en/spectrum-scale/5.0.5?topic=firewall-examples-how-open-ports)  
[https://forum.ubuntuusers.de/topic/var-run-dbus-system-bus-socket-no-such-file-o/](https://forum.ubuntuusers.de/topic/var-run-dbus-system-bus-socket-no-such-file-o/)


# Common Problems
## User does not have autocomplete
Check if the shell supports autocomplete (e.g. `/bin/bash` does, `/bin/sh` does not) and change it (`chsh -s /bin/bash <user>`) [reference](https://serverfault.com/a/99791)

## Pissing Permissions on rwxrwxrwx
home directory may be mounted 'incorrectly' (depending on your OS), move to non-home dir [ref](https://unix.stackexchange.com/questions/609166/bash-java-permission-denied)

## CURL
[https://stackoverflow.com/questions/12667797/using-curl-to-upload-post-data-with-files](ref)
```js
curl -F "image=@img.jpg" localhost:8080/upload
```
