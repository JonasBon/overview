Kubernetes with GitLab
======================

1.
```
# 1. (once) Login to Docker Registry
docker login registry.gitlab.com -u <your gitlab username> -p <your gitlab password or access-token (may additinally require read_api)>
# **unsave**, but seems to be the only working way

# 2. Build your image
docker build -t registry.gitlab.com/<namespace>/<repo>[:<tag>] .

# Optionally, test you image
docker run registry.gitlab.com/<namespace>/<repo>[:<tag>]

# 3. Push your image
docker push registry.gitlab.com/<namespace>/<repo>[:<tag>]

# 4. Run your pod
kubectl run <pod-name> --image=registry.gitlab.com/<namespace>/<repo>[:<tag>]
```

Kubernetes Locally
==================

[basically this](https://stackoverflow.com/a/48999680/11500248)
```
# Start minikube
minikube start

# Set docker env
eval $(minikube docker-env)             # unix shells
minikube docker-env | Invoke-Expression # PowerShell

# Build image
docker build -t foo:0.0.1 .

# Run in minikube
kubectl run hello-foo --image=foo:0.0.1 --image-pull-policy=Never

# Check that it's running
kubectl get pods
```
