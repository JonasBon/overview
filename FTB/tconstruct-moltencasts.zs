import mods.tconstruct.Casting;

print('Adding molten stuff.');
//energetic alloy
Casting.addBasinRecipe(<EnderIO:blockIngotStorage:1>, <liquid:energetic.alloy.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<EnderIO:itemAlloy:1>, <liquid:energetic.alloy.molten> * 144, <TConstruct:metalPattern>, false, 20);

//electrical steel
Casting.addBasinRecipe(<EnderIO:blockIngotStorage:0>, <liquid:electrical.steel.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<EnderIO:itemAlloy:0>, <liquid:electrical.steel.molten> * 144, <TConstruct:metalPattern>, false, 20);

//redstone alloy
Casting.addBasinRecipe(<EnderIO:blockIngotStorage:3>, <liquid:redstone.alloy.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<EnderIO:itemAlloy:3>, <liquid:redstone.alloy.molten> * 144, <TConstruct:metalPattern>, false, 20);

//soularium
Casting.addBasinRecipe(<EnderIO:blockIngotStorage:7>, <liquid:soularium.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<EnderIO:itemAlloy:7>, <liquid:soularium.molten>  * 144, <TConstruct:metalPattern>, false, 20);

//conductive iron
Casting.addBasinRecipe(<EnderIO:blockIngotStorage:4>, <liquid:conductive.iron.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<EnderIO:itemAlloy:4>, <liquid:conductive.iron.molten> * 144, <TConstruct:metalPattern>, false, 20);

//dark steel
Casting.addBasinRecipe(<EnderIO:blockIngotStorage:6>, <liquid:dark.steel.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<EnderIO:itemAlloy:6>, <liquid:dark.steel.molten> * 144, <TConstruct:metalPattern>, false, 20);

//manasteel
Casting.addBasinRecipe(<Botania:storage:0>, <liquid:manasteel.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<Botania:manaResource:0>,<liquid:manasteel.molten> * 144, <TConstruct:metalPattern>, false, 20);

//terrasteel
Casting.addBasinRecipe(<Botania:storage:1>, <liquid:terrasteel.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<Botania:manaResource:4>, <liquid:terrasteel.molten> * 144, <TConstruct:metalPattern>, false, 20);

//elementium
Casting.addBasinRecipe(<Botania:storage:2>, <liquid:elementium.molten> * 1296, null, false, 80);
Casting.addTableRecipe(<Botania:manaResource:7>,  <liquid:elementium.molten>  * 144, <TConstruct:metalPattern>, false, 20);
