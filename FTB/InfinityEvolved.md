(Client and Server) - (Settings | Options > Open Folder)
config>Avaritia.cfg
```
general.B:"Crafting Only"=false
```

Paste [./tconstruct-moltencasts.zs](tconstruct-moltencasts.zs) to `/modpack/normal/scripts/` (**not** `/scripts`)
```
wget https://gitlab.com/torbeneims/overview/-/raw/master/FTB/tconstruct-moltencasts.zs -P modpack/normal/scripts/
```

Added mod `NodalMechanics-1.7-1.0-7.jar` to `mods`
